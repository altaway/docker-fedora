FROM fedora:latest

# If usermod or useradd (instead of gpasswd here) is used to add the user to wheel, 
# the user will have to relogin for the changes to take place.
# "gpasswd -a $USER wheel" does it in-place.
# However, it is unnecessary to deal with horrors like usermod, gpasswd, visudo or something like that.
# Since this is a container not something persistent, just edit the sudoers file in-place.
RUN dnf -y install make which sudo zsh stow podman git-core passwd hostname; \
dnf -y clean all; \
SHELL=zsh; \
USER=user; \
HOME=/home/$USER; \
useradd -ms `which $SHELL` $USER; \
zsh -c "echo \"\n$USER ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers"; \
zsh -c "echo \"\npermit nopass keepenv user\" >> /etc/doas.conf"

SHELL ["zsh", "-c"]
USER user
WORKDIR /home/user

RUN DOTFILES=$HOME/.dotfiles; \
git clone https://gitlab.com/altaway/dotfiles.git $DOTFILES; \
pushd $DOTFILES; \
stow -t $HOME -R .; \
doas stow -t /root -R .; \
popd; \
sh <(curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs) -y
